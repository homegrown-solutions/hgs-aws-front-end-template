
## Example HGS Front-End Project
##### Shishir Tandale
###### v1.0.0

----------

You can view the [live example site](https://mak-chemical-dev.hmgrwn.app) to see how the different components interact with each other.

### Dependencies

- `npm`
- `aws-cli` (for deployments only)

### Usage

1. `npm install` to pull front-end Dependencies
1. `npm start` to run dev-server on port 3000
1. `npm run build` to build production binaries
1. `./deploy.sh` to build and deploy to a configured S3 site (assuming `aws-cli` is on your classpath)

### API Reference

Final documentation for the API is still In Progress. The following information may change with respect to final requirements.

##### API Root
- https://mak-chemical.hmgrwn.app/api-dev/


##### Available Resources and Methods
- `GET /blog`
    - Gets list of all blog posts
- `GET /news`
    - Gets list of all news articles
- `POST /test`
    - Returns request and user info
    - Authenticated


##### Proposed Resources and Methods
- `GET /product`
    - Get list of all products
- `POST /contactus`
    - Record contact us request
- `POST /blog`
    - Add new blog post
    - Authenticated
- `POST /news`
    - Add new news article
    - Authenticated
- `POST /product`
    - Add new product
    - Authenticated
- `DELETE /blog`
    - Delete blog post
    - Authenticated
- `DELETE /news`
    - Delete news article
    - Authenticated
- `DELETE /product`
    - Delete product
    - Authenticated
- `PATCH /blog`
    - Update blog post
    - Authenticated
- `PATCH /news`
    - Update news article
    - Authenticated
- `PATCH /product`
    - Update product
    - Authenticated
