import React, {PureComponent, Fragment} from 'react';
import reactn, {addReducer} from 'reactn';
import {Route, Switch, BrowserRouter, withRouter, Redirect} from 'react-router-dom';
import {hot} from 'react-hot-loader';

import Home from './pages/Home';
import Admin from './pages/Admin';
import Blog from './pages/Blog';
import News from './pages/News';

import AuthenticationService from './services/AuthenticationService';

import './spa.scss';

const authService = new AuthenticationService();

const Login = ({}) => {
  return (
    <span>Redirecting...</span>
  );
};

const Logout = ({}) => {
  authService.logout();
  return (
    <Redirect to="/"/>
  );
};

const Authenticate = ({}) => {
  return (
    <Redirect to="/"/>
  );
};

class SinglePageApp extends reactn.PureComponent {
  constructor(props) {
    super(props);
  }

  restrictPage = (component) => {
    if(authService.isAuthenticated()) {
      return component;
    } else {
      return component.Failed;
    }
  };

  render() {
    return (
      <Fragment>
        <main>
          <Switch location={this.props.location}>
            <Route exact path="/" component={Home}/>
            <Route path="/authenticate" component={Authenticate}/>
            <Route path="/logout" component={Logout}/>
            <Route path="/admin" component={this.restrictPage(Admin)}/>
            <Route path="/blog" component={Blog}/>
            <Route path="/news" component={News}/>
          </Switch>
        </main>
      </Fragment>
    );
  }
}

const SPAWithRouter = withRouter(SinglePageApp);

const RoutedApp = () => (
  <BrowserRouter>
    <SPAWithRouter/>
  </BrowserRouter>
);

export default hot(module)(RoutedApp);
