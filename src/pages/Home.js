import React, {PureComponent, Fragment} from 'react';
import reactn from 'reactn';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import Navbar from '../components/Navbar';

const Home = () => (
  <Fragment>
    <Navbar/>
    <h2>Home Page</h2>
    <div>
      <span>
        This is an example home page.
      </span>
    </div>
  </Fragment>
);

export default Home;
