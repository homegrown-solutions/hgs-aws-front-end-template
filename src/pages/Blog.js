import React, {PureComponent, Fragment} from 'react';
import reactn from 'reactn';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import Navbar from '../components/Navbar';
import ResourceTable from '../components/ResourceTable';

import Resource from '../services/Resource';
import BlogService from '../services/BlogService';

const blogResource = new Resource(new BlogService());

const Blog = () => (
  <Fragment>
    <Navbar/>
    <h2>Blog</h2>
    <div>
      <ResourceTable resource={blogResource}/>
    </div>
  </Fragment>
);

export default Blog;
