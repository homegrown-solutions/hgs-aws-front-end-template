import React, {PureComponent, Fragment} from 'react';
import reactn from 'reactn';
import PropTypes from 'prop-types';

import Navbar from '../components/Navbar';

import AuthenticationService from '../services/AuthenticationService';

const authService = new AuthenticationService();

const Admin = () => (
  <Fragment>
    <Navbar/>
    <h2>Admin Page</h2>
    <div>
      <span>
        This is an example of a secured admin page. This page can only be
        accessed while logged in.
      </span>
    </div>
    {authService.isAuthenticated() && (
      <div>
        <p>Welcome, {authService.userInfo.name}.</p>
        <p>User Details: <br/> {JSON.stringify(authService.userInfo)}</p>
      </div>
    )}
  </Fragment>
);

Admin.Failed = () => (
  <Fragment>
    <Navbar/>
    <p>You are not logged in.</p>
  </Fragment>
);

export default Admin;
