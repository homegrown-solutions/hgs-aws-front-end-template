import React, {PureComponent, Fragment} from 'react';
import reactn from 'reactn';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import Navbar from '../components/Navbar';
import ResourceTable from '../components/ResourceTable';

import Resource from '../services/Resource';
import NewsService from '../services/NewsService';

const newsResource = new Resource(new NewsService());

const News = () => (
  <Fragment>
    <Navbar/>
    <h2>News</h2>
    <div>
      <ResourceTable resource={newsResource}/>
    </div>
  </Fragment>
);

export default News;
