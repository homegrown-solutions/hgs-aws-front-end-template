
/*
  A Service is a singleton class. You can wrap a Class reference with
  serviceProxy in order to override the default constructor behavior.
  This allows for using the same Service instance across multiple contexts
  without having to worry about storing instance references globally.
*/

//exposes a service class as a singleton object
export const serviceProxy = (serviceClass) => {
  let instance = null;
  return new Proxy(serviceClass, {
    construct: (target, args) => {
      if(!instance) {
        instance = new target(...args);
      }
      return instance;
    }
  });
};
