
import { jsonFetch } from '../util/FetchUtil';
import { serviceProxy } from './Service';

import SystemService from './SystemService';

const Cookies = require('js-cookie');
const jwt = require('jsonwebtoken');

const systemService = new SystemService();

class AuthenticationService {
  cookieKey = 'makchemical-tokens';
  clientId = 'el88eoivda7mmb9u8cq1ta381';
  redirectUri = `${window.location.origin}/authenticate`;
  baseAuthUrl = 'https://makchemical-hgs.auth.us-east-1.amazoncognito.com/login';
  authParameters = () => ({
    response_type: 'token',
    client_id: this.clientId,
    redirect_uri: this.redirectUri,
    state: Date.now(),
    scope: ['profile','openid'].join('+')
  });

  //assigned at runtime
  tokens = null;
  authUrl = null;
  //helper function to check authentication state
  isAuthenticated = () => this.tokens && this.tokens!=false && this.tokens.id!=false && this.tokens.access!=false;

  constructor() {
    //get authentication information if available
    this.authenticate();
    //build auth url string
    this.buildAuthUrl();
  }

  //build auth url string at service startup
  buildAuthUrl = () => {
    this.authUrl = this.baseAuthUrl+'?'+
      Object.entries(this.authParameters())
        .map(([key, value])=>`${key}=${value}`)
        .join('&');
  }

  authenticate = () => {
    //check session cookie for tokens
    const existingTokens = Cookies.getJSON(this.cookieKey);
    if(existingTokens) {
      //if they exist, save and parse them
      this.tokens = existingTokens;
      this.parseToken();
    } else {
      //check current url for token information, update if necessary
      this.checkUrlForToken();
    }

  }

  //check current browser url for included tokens from login page
  checkUrlForToken = () => {
    const pathname = window.location.href;
    if(pathname.includes('#')) {
      //extract variables as js Map
      const idxOfHash = pathname.lastIndexOf('#');
      const urlMap = new Map(pathname.slice(idxOfHash+1).split('&').map(str=>str.split('=')));
      //store and process tokens
      if(urlMap.has('id_token') && urlMap.has('access_token')) {
        this.tokens = {
          id: urlMap.get('id_token'),
          access: urlMap.get('access_token'),
          expiresIn: urlMap.get('expires_in'),
          tokenType: urlMap.get('token_type'),
          state: urlMap.get('state')
        };
        this.parseToken();
        //save tokens in session cookie
        Cookies.set(this.cookieKey, this.tokens);
      }
    }
  };

  //extract and save user info from tokens
  parseToken = () => {
    if(this.isAuthenticated()) {
      this.userInfo = jwt.decode(this.tokens.id);
    }
  }

  //open login page in same window (it will redirect back to the app)
  login = () => {
    window.open(this.authUrl,"_self");
  }

  //to logout, just clear tokens from memory and cookies
  logout = () => {
    this.tokens = null;
    this.userInfo = null;
    Cookies.remove(this.cookieKey);
  }

  testAuthentication = () => {
    if(this.isAuthenticated()) {
      this.jsonFetchWithToken('https://mak-chemical.hmgrwn.app/api-dev/test', {
        method: 'GET'
      }).then(console.log);
    }
  }

  //helper method for other classes to use to send an authenticated json request
  jsonFetchWithToken = (url, options) => {
    if(this.isAuthenticated()) {
      if(!options.headers) options.headers = {};
      options.headers["Authorization"] = `Bearer ${this.tokens.access}`;
      return jsonFetch(url, options);
    } else {
      console.log("Error: Not authenticated.");
      return null;
    }
  };

}

export default serviceProxy(AuthenticationService);
