
import { jsonFetch } from '../util/FetchUtil';
import { serviceProxy } from './Service';
import AuthenticationService from './AuthenticationService';

const authService = new AuthenticationService();

class NewsService {
  serviceUrl = "https://mak-chemical.hmgrwn.app/api-dev/news";

  resourceAutoDiscovery = {
    entity: "news",
    supports: {
      //getById: true,
      getAll: true,
      add: true,
      update: true,
      delete: true
    },
    targets: (ctx) => ({
      //getById: ctx.getPost,
      getAll: ctx.getAllPosts,
      add: ctx.createPost,
      update: ctx.updatePost,
      delete: ctx.deletePost
    }),
    fields: [
      {key:'id', label:'Id', type:'string', sort: true},
      {key:'author', label:'Author', type:'string', sort: true},
      {key:'post', label:'Post', type:'string'},
      {key:'created_date', label:'Date', type:'string', sort: true}
    ]
  };

  cache = new Map();
  cacheFilledTime = 0;

  static formatPostDate = (postDate) => {
    if(postDate) {
      const date = new Date(postDate._seconds*1000);
      let numHours = (date.getHours()+1);
      let amPm = "AM";
      if(numHours>12) { numHours -= 12; amPm = "PM"; }
      return `${date.getMonth()+1}/${date.getDate()}/${date.getFullYear()} ${numHours}:${date.getMinutes()} ${amPm}`;
    } else {
      return "<never>";
    }
  }

  /*getPost = (id) => {
    //avoid fetch request if post is already in cache
    if(this.cache.has(id)) {
      return new Promise((resolve, reject) => {
        resolve(this.cache.get(id));
      });
    }

    const getPostUrl = `${this.serviceUrl}?get=${id}`;
    return secureFetch(getPostUrl, {method:"GET"})
      .then(res=>res.json())
      .then(res=>{
        if(res.error) throw res.error;
        this.cachePost(id, res);
        return res;
      })
      .catch(console.out);
  }*/

  getAllPosts = () => {
    if(this.cacheFilledTime && performance.now()-this.cacheFilledTime<(1000*60*10)) {
      return new Promise((resolve, reject) => {
        resolve([...this.cache.values()]);
      });
    }

    return fetch(this.serviceUrl, {
      method:'GET',
      headers: {
          'Accept': 'application/json'
      }
    }).then(res=>res.json())
      .then(res=>{
        if(res.error) throw res.error;
        res.data.forEach(post=>{this.cachePost(post.id, post);});
        this.cacheFilledTime = performance.now();
        return res.data;
      })
      .catch(console.out);
  }

  createPost = (post) => {
    if(authService.isAuthenticated()) {
      return authService.jsonFetchWithToken(this.serviceUrl, {
        method: "POST",
        body: {
          blogpost: post
        }
      }).then(res=>{
        if(res.postId) {
          return this.getPost(res.postId);
        }
        return res;
      })
      .catch(console.out);
    }
  }

  updatePost = (id, post) => {
    if(authService.isAuthenticated()) {
      return authService.jsonFetchWithToken(this.serviceUrl, {
        method: "PATCH",
        body: {
          postId: id,
          blogpost: post
        }
      }).then(res=>{
        if(res.success) {
          this.cache.delete(id);
          return this.getPost(id);
        }
        return res;
      })
      .catch(console.out);
    }
  }

  deletePost = (id) => {
    if(authService.isAuthenticated()) {
      return authService.jsonFetchWithToken(this.serviceUrl, {
        method: "DELETE",
        body: {
          postId: id
        }
      }).then(res=>{
        if(res.success) {
          this.cache.delete(id);
        }
        return res;
      })
      .catch(console.out);
    }
  }

  cachePost = (id, post) => {
    this.cache.set(id, post);
  };

}

export default serviceProxy(NewsService);
