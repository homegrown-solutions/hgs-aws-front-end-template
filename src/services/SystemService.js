
import { serviceProxy } from './Service';

//utility function to update meta tag content by name
function updateMetaContent(name, content) {
  document.querySelector(`meta[name="${name}"]`).setAttribute('content', content);
}

class SystemService {
  nightTime = {start: 7+12, end: 7};

  isNightTime = () => {
    const curDate = new Date();
    //nightMode = true between nightTime.start and nightTime.end
    return (
      curDate.getHours()>=(this.nightTime.start)
      || curDate.getHours()<(this.nightTime.end)
    );
  };

  debug = (msg) => {
    console.log(`${new Date()} DEBUG :: ${msg}`);
  };

  useDarkMode = () => {
    //read prefers-color-scheme CSS media feature if it exists
    //otherwise, fall back to a time-based calculation
    return (
      window.matchMedia('(prefers-color-scheme: dark)').matches
      || this.isNightTime()
    );
  };

  setBrowserThemeColor = (nightMode) => {
    const newColor = (nightMode) ? '#000000' : '#f8f9fa';
    updateMetaContent('theme-color', newColor);
    updateMetaContent('msapplication-navbutton-color', newColor);
    updateMetaContent('apple-mobile-web-app-status-bar-style', newColor);
  };

}

export default serviceProxy(SystemService);
