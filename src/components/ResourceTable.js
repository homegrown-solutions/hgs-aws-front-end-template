import React, {PureComponent, Fragment} from 'react';
import PropTypes from 'prop-types';
import SpicyDatatable from 'spicy-datatable';

import './ResourceTable.scss';

class ResourceTable extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      columns: props.resource.fields,
      rows: []
    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    this.props.resource.getAll()
      .then(res=>{
        this.setState((prevState)=>({
           rows: res
        }));
      });
  }

  render() {
    return (
      <SpicyDatatable
        tableKey={`${this.props.resource.entity}-datatable`}
        columns={this.state.columns}
        rows={this.state.rows}
      />
    );
  }
}
ResourceTable.propTypes = {
  resource: PropTypes.object.isRequired
}

export default ResourceTable;
