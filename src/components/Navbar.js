import React, {Fragment, PureComponent} from 'react';
import {Link} from 'react-router-dom';
import AuthenticationService from '../services/AuthenticationService';

const authService = new AuthenticationService();

import './Navbar.scss';

const navLinks = [
  {name:"Home", path:"/"},
  {name:"Blog", path:"/blog"},
  {name:"News", path:"/news"},
  {name:"Products"},
  {name:"Admin", path:"/admin", authenticated: true},
  {name:"Login", path:authService.authUrl, authenticated: false},
  {name:"Logout", path:"/logout", authenticated: true}
];

class Navbar extends PureComponent {
  render() {
    return (
      <nav>
        <ul>
          {navLinks
            .filter(navLink => (
                navLink.authenticated===undefined ||
                (navLink.authenticated===true && authService.isAuthenticated()) ||
                (navLink.authenticated===false && !authService.isAuthenticated())
            ))
            .map(navLink => (
              <li key={`navlink-${navLink.name}`}>
                {(navLink.path) ? (
                  (navLink.path.startsWith('/')) ? (
                    <Link to={navLink.path}>{navLink.name}</Link>
                  ) : (
                    <a href={navLink.path}>{navLink.name}</a>
                  )
                ) : (
                  navLink.name
                )}
              </li>
            ))
          }
        </ul>
      </nav>
    );
  }
}

export default Navbar;
