
//helper function for dealing with json encoded data
export const jsonFetch = (url, options={}) => {
  if(!options.headers) options.headers = {};
  options.headers["Content-Type"] = "application/json";
  options.body = JSON.stringify(options.body);
  return fetch(url, options).then(res=>res.json());
};
