import "@babel/polyfill";

import React from "react";
import ReactDOM from "react-dom";

import SinglePageApp from "./spa";

ReactDOM.render((
    <SinglePageApp/>
  ),
  document.getElementById("root")
);
