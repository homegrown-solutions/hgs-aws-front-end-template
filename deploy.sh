#!/usr/bin/env bash

# remove previously built files
rm public/*

# build new files
npm run build

# delete old files on AWS
aws s3 rm s3://mak-chemical-dev-1.homegrown-solutions.com/ --recursive

# copy over new files to AWS
aws s3 cp public/ s3://mak-chemical-dev-1.homegrown-solutions.com --recursive \
 --metadata-directive REPLACE --content-encoding "gzip" \
 --cache-control "public, max-age=31536000" --acl "public-read"

# print out files currently on AWS
aws s3 ls s3://mak-chemical-dev-1.homegrown-solutions.com/

echo 'Completed deployment!'
