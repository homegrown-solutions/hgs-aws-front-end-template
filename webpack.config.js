const path = require("path");
const webpack = require("webpack");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, argv) => {
  const devMode = (argv.mode!=="production");

  return {
    entry: {
        spa: "./src/spaEntrypoint.js"
    },
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
      splitChunks: {
  			cacheGroups: {
  				vendor: {
  					test: /node_modules/,
  					chunks: "all",
  					name: "vendor"
  				}
  			}
		  }
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader'
        },
        { //process and hash all media filenames
          test: /\.(png|jpe?g|gif|mp4|svg|mov)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                publicPath: '/dist/assets/',
                outputPath: 'assets/'
              }
            }
          ]
        },
        {
          test: /\.css$/,
          use: [ MiniCssExtractPlugin.loader,
              'css-loader', 'postcss-loader' ]
        },
        {
          test: /\.scss$/,
          use: [ MiniCssExtractPlugin.loader,
              'css-loader', 'postcss-loader', 'sass-loader' ]
        }
      ]
    },
    output: {
      path: path.resolve(__dirname, "public/"),
      filename: "[name].[hash].js"
    },
    devServer: {
      contentBase: path.join(__dirname, "public/"),
      port: 3000,
      publicPath: "http://localhost:3000/",
      historyApiFallback: true,
      headers: {
        //required when used with CompressionPlugin
        'Content-Encoding': 'gzip'
      },
      hotOnly: true
    },
    resolve: {
      alias: {
        'react-dom': '@hot-loader/react-dom'
      }
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: "HGS Website",
        template: "./src/templates/indexTemplate.html",
        defaultThemeColor: "#000000",
        minify: {
          collapseWhitespace: true,
          minifyJS: true
        },
        /*googleAnalytics: {
          trackingId: "<insert-trackingId-here>"
        }*/
      }),
      new MiniCssExtractPlugin({
        filename: "[name].[hash].css"
      }),
      new CompressionPlugin({
        test: /\.(js|css|html)$/,
        filename: '[path][query]',
        algorithm: 'gzip'
      }),
      new webpack.HotModuleReplacementPlugin()
    ]
  };
};
